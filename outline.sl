#!/usr/bin/env slsh
require("expat");

define startElement(p, name, atts) {
   variable i;
   ()=printf("%*s%s\n", p.userdata, "", name);
   foreach i (atts)
     {
	()=printf("%*s%s=%s;\n", p.userdata, "", i.name, i.value);
     }
   p.userdata++;
}                                                                          


define endElement (p, name)
{
   p.userdata--;
}

define slsh_main()
{
   !if (__argc)
     {
	() = fprintf (stderr, "Usage: %s [xml file]\n", __argv[0]);
	exit (1);
     }
   
   variable file = __argv[1];
   variable xml_parser = xml_new();
   
   variable data;
   xml_set_user_data(xml_parser, 0);
   xml_parser.startelementhandler = &startElement;
   xml_parser.endelementhandler = &endElement;
   
   variable fp = fopen(file, "r");
   if (fp == NULL)
     verror("could not open XML input");
   while (-1 != fread_bytes(&data, 4096, fp))
     xml_parse(xml_parser, data, feof(fp));
}

