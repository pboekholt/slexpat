#s+
custom_variable("tm_format", "slhlp");
if (tm_format == "html")
{
   tm_add_macro("vdt", "\\dtdd{\\code{$1}}{$2}", 2, 2);
   tm_add_macro("n", "\\newline", 0, 0);
   tm_add_macro("link", "\\href{#$1}{$1}", 1, 1);
}
else
{
   tm_add_macro("dl", "$1", 1, 1);
   tm_add_macro("vdt", "\\var{$1}: $2\\__newline__", 2, 2);
   tm_add_macro("n", "", 0, 0);
   tm_add_macro("link", "$1", 1, 1);
}
#s-

\function{xml_new}
\synopsis{Instantiate a new Expat_Type object}
\usage{Expat_Type xml_new()}
\description
  This function instantiates and returns an \dtype{Expat_Type}.  The
  \dtype{Expat_Type} is an opaque type, but its internals can be accessed
  with the dot operator like in a structure:
  \dl{
  \vdt{.userdata}{see \ifun{\link{xml_set_user_data}}}
  \vdt{.startelementhandler}{see \ifun{\link{xml_set_start_element_handler}}}
  \vdt{.endelementhandler}{see \ifun{\link{xml_set_end_element_handler}}}
  \vdt{.characterdatahandler}{see \ifun{\link{xml_set_character_data_handler}}}
  \vdt{.defaulthandler}{see \ifun{\link{xml_set_default_handler}}}
  }
  The following properties do nothing unless the Expat parser was created
  with \ifun{\link{xml_new_ns}}:
  \dl{
  \vdt{.startnamespacedeclhandler}{ see \ifun{\link{xml_set_start_namespace_decl_handler}}}
  \vdt{.endnamespacedeclhandler}{ see \ifun{\link{xml_set_end_namespace_decl_handler}}}
  }
  The following properties are read-only and correspond to the C functions
  XML_GetErrorCode etc:
  \dl{
  \vdt{.errorcode}{the error status of the parser.  This will be
  \exc{ExpatError} or a subclass thereof, not expat's internal error
  code.}
  \vdt{.errorstring}{the description of the error status.}
  \vdt{.currentbyteindex}{the byte offset of the position.}
  \vdt{.currentline}{ the line number of the position. The first line is
  reported as 1.}
  \vdt{.currentcolumn}{the offset, from the beginning of the current line, of the
   position.}
  \vdt{.parsingstatus}{a struct {\dtype{Int_Type} parsing, \dtype{Int_Type}
  finalbuffer}.
   \var{.parsing.parsingstatus} can take on four values:
#v+
     XML_INITIALIZED
     XML_PARSING
     XML_FINISHED
     XML_SUSPENDED
#v-
   \var{.parsing.finalbuffer} is one if the final buffer is being processed -
   that is, when \ifun{xml_parse} was called with the third argument 1.
   }}
\seealso{xml_parse, xml_new_ns}
\done

\function{xml_new_ns}
\synopsis{Instantiate an Expat_Type object with namespace processing}
\usage{Expat_Type xml_new_ns(Char_Type sep)}
\description
  Constructs a new parser that has namespace processing in effect. Namespace
  expanded element names and attribute names are returned as a concatenation
  of the namespace URI, sep, and the local part of the name. This means that
  you should pick a character for sep that can't be part of a legal URI. 
\seealso{xml_parse, xml_new}
\done

\function{xml_parse}
\synopsis{parse part of and xml document}
\usage{xml_parse(Expat_Type p, String_Type s, Int_Type isFinal)}
\description
  Parse some more of the document.  The string s is a buffer containing part
  (or perhaps all) of the document.  The isFinal parameter informs the parser
  that this is the last piece of the document.  Frequently, the last piece is
  empty (i.e. len is zero).  If a parse error occurred, an \exc{ExpatError}
  is thrown.
\seealso{xml_new, xml_stop_parser}
\done


\function{xml_stop_parser}
\synopsis{stop parsing}
\usage{xml_stop_parser(Expat_Type p, Int_Type resumable)}
\description
  Stops parsing, causing \ifun{xml_parse} to return. Must be called from
  within a call-back handler, except when aborting (when resumable is 0) an
  already suspended parser.
\seealso{xml_new, xml_resume_parser}
\done


\function{xml_resume_parser}
\synopsis{resume parsing}
\usage{xml_resume_parser(Expat_Type p)}
\description
  Resumes parsing after it has been suspended with \ifun{xml_stop_parser}.
  Must not be called from within a handler call-back.
\seealso{xml_new, xml_stop_parser}
\done


\function{xml_set_start_element_handler}
\synopsis{set the start element handler}
\usage{xml_set_start_element_handler(Expat_Type p, Ref_Type handler)}
\description
  Set the handler for start elements.  The handler should have the prototype
#v+
  handler(p, name, atttributes)
  Expat_Type p;
  String_Type name;
  Array_Type[Struct_Type{name, value}] atttributes;
#v-
  The handler can acces the parser's userdata with
  \exmp{xml_get_user_data(p)} or as p.userdata.
\notes
  To unset the handler, set it to \NULL
\seealso{xml_new, xml_parse}
\done


\function{xml_set_end_element_handler}
\synopsis{set the end element handler}
\usage{xml_set_end_element_handler(Expat_Type p, Ref_Type handler)}
\description
  Set the handler for end elements.  The handler should have the prototype
#v+
  handler(p, name)
  Expat_Type p;
  String_Type name;
#v-
  The handler can acces the parser's userdata with
  \exmp{xml_get_user_data(p)} or as p.userdata.
\notes
  To unset the handler, set it to \NULL
\seealso{xml_new, xml_parse}
\done

\function{xml_set_start_namespace_decl_handler}
\synopsis{set the start namespace_decl handler}
\usage{xml_set_start_namespace_decl_handler(Expat_Type p, Ref_Type handler)}
\description
  Set a handler to be called when a namespace is declared. Namespace
  declarations occur inside start tags. But the namespace declaration start
  handler is called before the start tag handler for each namespace declared
  in that start tag.  The handler should have the prototype
#v+
  handler(p, prefix, uri)
  Expat_Type p;
  String_Type  or NULL prefix;
  String_Type or NULL uri;
#v-
\notes
  To unset the handler, set it to \NULL
\seealso{xml_new_ns, xml_parse}
\done

\function{xml_set_end_namespace_decl_handler}
\synopsis{set the end namespace_decl handler}
\usage{xml_set_end_namespace_decl_handler(Expat_Type p, Ref_Type handler)}
\description
  Set a handler to be called when leaving the scope of a namespace
  declaration.  This will be called, for each namespace declaration, after
  the handler for the end tag of the element in which the namespace was
  declared.  The handler should have the prototype
#v+
  handler(p, prefix)
  Expat_Type p;
  String_Type or NULL prefix;
#v-
\notes
  To unset the handler, set it to \NULL
\seealso{xml_new_ns, xml_parse}
\done

\function{xml_set_character_data_handler}
\synopsis{set the character data handler}
\usage{xml_set_character_data_handler(Expat_Type p, Ref_Type handler)}
\description
  Set the handler for character data.  The handler should have the prototype
#v+
  handler(p, data)
  Expat_Type p;
  String_Type data;
#v-
  The handler can acces the parser's userdata with
  \exmp{xml_get_user_data(p)} or as p.userdata.
\notes
  To unset the handler, set it to \NULL
\seealso{xml_new, xml_parse}
\done

\function{xml_set_default_handler}
\synopsis{set the character data handler}
\usage{xml_set_default_handler(Expat_Type p, Ref_Type handler)}
\description
  Sets a handler for any characters in the document which wouldn't otherwise
  be handled. The handler should have the prototype
#v+
  handler(p, data)
  Expat_Type p;
  String_Type data;
#v-
\notes
  To unset the handler, set it to \NULL
\seealso{xml_new, xml_parse}
\done

\function{xml_set_user_data}
\synopsis{Set the user data associated with an XML parser}
\usage{xml_set_user_data(Expat_Type p, AnyType data)}
\description
  This sets the user data pointer that gets passed to handlers.
\seealso{xml_new, xml_get_user_data}
\done

\function{xml_get_user_data}
\synopsis{Get the user data associated with an XML parser}
\usage{AnyType xml_get_user_data(Expat_Type p)}
\description
  This gets the user data pointer that gets passed to handlers.
\seealso{xml_new, xml_set_user_data}
\done
